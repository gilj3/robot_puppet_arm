/*
----Robot Puppet Arm---------------------------------------------------------
Code Upgrade for lib OpenNI 2.0 Chapter 7 book of Gred Borenstein  
Making Things See 3D vision with Kinect, Processing, Arduino, and MakerBot
DATE: 03-10-2014
Gil jr <giljr.2009@gmail.com>
---------------------------------------------------------------------------
*/
#include <Servo.h> //1
// Chapter 6 Making things sees - Arm controler´s
//declare both servos
Servo shoulder;
Servo elbow;
// setup the array of servo positions //2
int nextServo = 0;
int servoAngles[] = {
  0,0};
void setup() {
  // attach servos to their pins //3
  shoulder.attach(9);
  elbow.attach(10);
  Serial.begin(9600);
}
void loop() {
  if(Serial.available()){ //4
    int servoAngle = Serial.read();
    servoAngles[nextServo] = servoAngle;
    nextServo++;
    if(nextServo > 1){
      nextServo = 0;
    }
    shoulder.write(servoAngles[0]); //5    
    elbow.write(servoAngles[1]);    
  }
}

