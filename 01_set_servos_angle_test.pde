import processing.serial.*; //1
Serial port;
void setup() {
  println(Serial.list()); //2
  String portName = Serial.list()[0];
  port = new Serial(this, portName, 9600); //3
}
void draw() {
} //4
void keyPressed() { //5
  if (key == 'a') {
    port.write(0);
  }
  if (key == 's') {
    port.write(90);
  }
  if (key == 'd') {
    port.write(180);
  }
}

