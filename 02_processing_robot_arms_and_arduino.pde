/*
----Robot Puppet Arm---------------------------------------------------------
Code Upgrade for lib OpenNI 2.0 Chapter 7 book of Gred Borenstein  
Making Things See 3D vision with Kinect, Processing, Arduino, and MakerBot
DATE: 03-10-2014
Gil jr <giljr.2009@gmail.com>
---------------------------------------------------------------------------
*/

import SimpleOpenNI.*;
SimpleOpenNI kinect;
// import the processing serial library
import processing.serial.*;
// and declare an object for our serial port
Serial port;
void setup() {
  size(640, 480);
  kinect = new SimpleOpenNI(this);
  if (kinect.isInit() == false)
  {
    println("Can't init SimpleOpenNI, maybe the camera is not connected!"); 
    exit();
    return;
  }
  kinect.enableDepth();
  kinect.enableUser();
  kinect.setMirror(true);
 
  println(Serial.list());
  String portName = Serial.list()[0];
  // initialize our serial object with this port
  // and the baud rate of 9600
  port = new Serial(this, portName, 9600);
}
void draw() {
  kinect.update();
  PImage depth = kinect.depthImage();
  image(depth, 0, 0);
  IntVector userList = new IntVector();
  kinect.getUsers(userList);
  if (userList.size() > 0) {
    int userId = userList.get(0);
    if ( kinect.isTrackingSkeleton(userId)) {
      // get the positions of the three joints of our arm
      PVector rightHand = new PVector();
      kinect.getJointPositionSkeleton(userId, 
      SimpleOpenNI.SKEL_RIGHT_HAND, rightHand);
	  
      PVector rightElbow = new PVector();
      kinect.getJointPositionSkeleton(userId, 
      SimpleOpenNI.SKEL_RIGHT_ELBOW, rightElbow);
	  
      PVector rightShoulder = new PVector();
      kinect.getJointPositionSkeleton(userId, 
      SimpleOpenNI.SKEL_RIGHT_SHOULDER, rightShoulder);
	  
      // convert our arm joints into screen space coordinates
      PVector convertedRightHand = new PVector();
      kinect.convertRealWorldToProjective(rightHand, convertedRightHand);
      PVector convertedRightElbow = new PVector();
      kinect.convertRealWorldToProjective(rightElbow, convertedRightElbow);
      PVector convertedRightShoulder = new PVector();
      kinect.convertRealWorldToProjective(rightShoulder, 
      convertedRightShoulder);
	  
      // we need right hip to orient the shoulder angle
      PVector rightHip = new PVector();
      kinect.getJointPositionSkeleton(userId, 
      SimpleOpenNI.SKEL_RIGHT_HIP, rightHip);
	  
      // reduce our joint vectors to two dimensions
      PVector rightHand2D = new PVector(rightHand.x, rightHand.y);
      PVector rightElbow2D = new PVector(rightElbow.x, rightElbow.y);
      PVector rightShoulder2D = new PVector(rightShoulder.x, 
      rightShoulder.y);
      PVector rightHip2D = new PVector(rightHip.x, rightHip.y);
	  
      // calculate the axes against which we want to measure our angles
      PVector torsoOrientation = PVector.sub(rightShoulder2D, rightHip2D);
      PVector upperArmOrientation =
        PVector.sub(rightElbow2D, rightShoulder2D);
		
      // calculate the angles of each of our arms
      float shoulderAngle =
        angleOf(rightElbow2D, rightShoulder2D, torsoOrientation);
      float elbowAngle =
        angleOf( rightElbow2D, rightHand2D, upperArmOrientation);
      // show the angles on the screen for debugging
      fill(255, 0, 0);
      scale(3);
      text("shoulder: " + int(shoulderAngle) + "\n" +
        " elbow: " + int(elbowAngle), 20, 20);
      byte out[] = new byte[2];
      out[0] = byte(shoulderAngle);
      out[1] = byte(elbowAngle);
      port.write(out);
      //println(out);//uncomment to debug
    }
  }
}

float angleOf(PVector one, PVector two, PVector axis) {
  PVector limb = PVector.sub(two, one);
  return degrees(PVector.angleBetween(limb, axis));
}

// user-tracking callbacks!
void onNewUser(SimpleOpenNI kinect, int userId) {
  println("start pose detection");
  kinect.startTrackingSkeleton(userId);
}


void onLostUser(SimpleOpenNI kinect, int userId)
{
  println("onLostUser - userId: " + userId);
}


